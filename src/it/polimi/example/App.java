package it.polimi.example;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MusicInstrument g1 = new MusicInstrument("Violino", 50);
		MusicInstrument g2 = new MusicInstrument("Arpa", 70);
		
		System.out.println(g1.getName());
		g2.play();
		g1.play("tanti auguri");
		
		Guitar g3 = new Guitar("Chitarra", 100);
		g3.setTuned(true);
		g3.play("tanti auguri");
	}

}
