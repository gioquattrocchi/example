package it.polimi.example;

public class Guitar extends MusicInstrument {

	private boolean tuned;
	
	public Guitar(String name, int price) {
		super(name, price);
	}

	public boolean isTuned() {
		return tuned;
	}

	public void setTuned(boolean tuned) {
		this.tuned = tuned;
	}
	
	@Override 
	public void play(String song){
		if(tuned){
			super.play(song);
		}
		else{
			System.out.println("Lo strumento "+this.getName()+" non è accordato. Non può suonare");
		}
	}
	
	

	
}
