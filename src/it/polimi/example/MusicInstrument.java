package it.polimi.example;

public class MusicInstrument {

	private String name;
	private int price;
	
	public MusicInstrument(String name, int price){
		this.name = name;
		this.price = price;
	}
	
	public void play() {
		System.out.println("Lo stumento "+name+" sta suonando");
	}
	
	public void play(String song){
		System.out.println("Lo stumento "+name+" sta suonando "+song);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
}
